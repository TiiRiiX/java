package ru.fnight.lab2.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Random;

import java.util.ResourceBundle;
import java.util.stream.Stream;

import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.fnight.lab2.FileUtils;
import ru.fnight.lab2.FileUtils.Flags;
import ru.fnight.lab2.Properties;
import ru.fnight.lab2.Properties.PropertieKeys;
import ru.fnight.lab2.SynsService;
import ru.fnight.oslab4.ImageUtils;

import static ru.fnight.lab2.App.CONFIG_FILE_NAME;

public class Window implements Initializable {

    public AnchorPane pane;

    private static ClassLoader classLoader = Window.class.getClassLoader();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.setProperty(SynsService.RMI_HOSTNAME, SynsService.LOCALHOST);
        SynsService.createServer(pane);
    }

    public static void addRandomImage(double x, double y, String filePath, AnchorPane pane) throws URISyntaxException {
        File file = new File(Window.class.getResource(filePath).toURI());
        try {
            System.out.println("class loaded: " +
                    classLoader.loadClass("ru.fnight.oslab4.ImageUtils").getName());
            ImageView imageView = new ImageView(ImageUtils.Companion.getJavaFxImage(file.getPath()));
            double width = imageView.getImage().getWidth();
            double height = imageView.getImage().getHeight();
            imageView.setX(Math.floor(x / width) * width);
            imageView.setY(Math.floor(y / height) * height);
            Platform.runLater(() -> pane.getChildren().add(imageView));
        } catch (ClassNotFoundException e) {
            System.out.println("lib not found");
        }
    }

    private void setColorByPart(double x, double y) {
        double halfHeight = pane.getHeight() / 2;
        double halfWidth = pane.getWidth() / 2;
        boolean isTop = y < halfHeight;
        boolean isBottom = y > halfHeight;
        boolean isRight = x > halfWidth;
        boolean isLeft = x < halfWidth;
        if (isTop && isLeft) {
            pane.setStyle("-fx-background-color: #FF0000");
        } else if (isRight && isTop) {
            pane.setStyle("-fx-background-color: #FFFF00");
        } else if (isLeft && isBottom) {
            pane.setStyle("-fx-background-color: #00FF00");
        } else if (isRight && isBottom) {
            pane.setStyle("-fx-background-color: #0000FF");
        }
    }

    public void click(MouseEvent mouseEvent) throws URISyntaxException {
        double x = mouseEvent.getX();
        double y = mouseEvent.getY();
        Random rand = new Random();
        String filePath = rand.nextBoolean() ? "/happy.png" : "/sad.jpg";
        addRandomImage(x, y, filePath, pane);
        SynsService.sayToCreateImage(x, y, filePath);
        setColorByPart(x, y);
    }

    private void setRandomColor() {
        Random random = new Random();
        int nextInt = random.nextInt(0xffffff + 1);
        String colorCode = String.format("#%06x", nextInt);
        pane.setStyle("-fx-background-color: " + colorCode);
    }

    public void setProperties(Properties properties, Stage stage) {
        if (properties.exist(PropertieKeys.WINDOW_X)) {
            stage.setX(Double.valueOf(properties.get(PropertieKeys.WINDOW_X)));
        }
        if (properties.exist(PropertieKeys.WINDOW_Y)) {
            stage.setY(Double.valueOf(properties.get(PropertieKeys.WINDOW_Y)));
        }
        if (properties.exist(PropertieKeys.WINDOW_WIDTH)) {
            pane.setPrefWidth(Double.valueOf(properties.get(PropertieKeys.WINDOW_WIDTH)));
        }
        if (properties.exist(PropertieKeys.WINDOW_HEIGHT)) {
            pane.setPrefHeight(Double.valueOf(properties.get(PropertieKeys.WINDOW_HEIGHT)));
        }
        if (properties.exist(PropertieKeys.BACKGROUND_COLOR)) {
            pane.setStyle("-fx-background-color: " + properties.get(PropertieKeys.BACKGROUND_COLOR));
        }
    }

    public void saveWindowPropertiesAndExit(Properties properties) {
        String fileMethod = properties.get(PropertieKeys.FILE_METHOD);
        Stage stage = (Stage) pane.getScene().getWindow();
        properties.put(PropertieKeys.WINDOW_X, Double.toString(stage.getX()));
        properties.put(PropertieKeys.WINDOW_Y, Double.toString(stage.getY()));
        properties.put(PropertieKeys.WINDOW_WIDTH, Double.toString(pane.widthProperty().doubleValue()));
        properties.put(PropertieKeys.WINDOW_HEIGHT, Double.toString(pane.heightProperty().doubleValue()));
        Stream.of(pane.getStyle().split(";")).forEach(s -> {
            String[] kv = s.split(":");
            if (kv.length > 1 && kv[0].contains("-fx-background-color")) {
                properties.put(PropertieKeys.BACKGROUND_COLOR, kv[1].trim());
            }
        });
        String strProperties = properties.toString();
        try {
            if (fileMethod != null) {
                switch (fileMethod) {
                    case Flags.MEMORY:
                        FileUtils.writeWithBuffer(CONFIG_FILE_NAME, strProperties);
                        break;
                    case Flags.VARS:
                        FileUtils.writeWithWriter(CONFIG_FILE_NAME, strProperties);
                        break;
                    case Flags.STREAMS:
                        FileUtils.writeWithStream(CONFIG_FILE_NAME, strProperties);
                        break;
                }
            } else {
                FileUtils.writeWithStream(CONFIG_FILE_NAME, strProperties);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SynsService.saveNeighbourWithoutYou(SynsService.PORT);
        stage.close();
        System.exit(0);
    }

    public void setKeyEvents(KeyEvent event) {
        Stage stage = (Stage) pane.getScene().getWindow();
        KeyCodeCombination combinationQCtrl = new KeyCodeCombination(KeyCode.Q, KeyCodeCombination.CONTROL_DOWN);
        KeyCodeCombination combinationShiftC = new KeyCodeCombination(KeyCode.C, KeyCodeCombination.SHIFT_DOWN);
        if (event.getCode() == KeyCode.ESCAPE || combinationQCtrl.match(event)) {
            stage.close();
        } else if (event.getCode() == KeyCode.ENTER) {
            setRandomColor();
        } else if (combinationShiftC.match(event)) {
            String osName = System.getProperty("os.name").toLowerCase();
            Runtime run = Runtime.getRuntime();
            try {
                if (osName.contains("linux")) {
                    run.exec("gedit");
                } else if (osName.contains("mac")) {
                    run.exec("open");
                } else {
                    run.exec("notepad");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
