package ru.fnight.lab2;

import javafx.scene.layout.AnchorPane;
import ru.fnight.lab2.controllers.Window;

import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class SynsRmiServiceImpl extends UnicastRemoteObject implements SynsRmiService {

    public static final String serviceName = "SynsRmiService";

    private AnchorPane pane;

    @Override
    public void createRandomImage(double x, double y, String filePath) throws URISyntaxException {
        Window.addRandomImage(x, y, filePath, pane);
    }

    SynsRmiServiceImpl(AnchorPane pane) throws RemoteException {
        super();
        this.pane = pane;
    }
}
