package ru.fnight.lab2;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ru.fnight.lab2.FileUtils.Flags;
import ru.fnight.lab2.Properties.PropertieKeys;
import ru.fnight.lab2.controllers.Window;

import java.util.Arrays;
import java.util.List;

public class App extends Application {

    private static Properties properties = null;

    public static String CONFIG_FILE_NAME = "config";
    public static String PORTS_FILE_NAME = "ports";

    private static void initProperties(String[] args) throws Exception {
        List<String> argList = Arrays.asList(args);
        if (argList.contains(Flags.MEMORY)) {
            properties = new Properties(FileUtils.read(CONFIG_FILE_NAME, Flags.MEMORY));
            properties.put(PropertieKeys.FILE_METHOD, Flags.MEMORY);
        } else if (argList.contains(Flags.VARS)) {
            properties = new Properties(FileUtils.read(CONFIG_FILE_NAME, Flags.VARS));
            properties.put(PropertieKeys.FILE_METHOD, Flags.VARS);
        } else if (argList.contains(Flags.STREAMS)) {
            properties = new Properties(FileUtils.read(CONFIG_FILE_NAME, Flags.STREAMS));
            properties.put(PropertieKeys.FILE_METHOD, Flags.STREAMS);
        } else {
            // default method
            properties = new Properties(FileUtils.read(CONFIG_FILE_NAME, Flags.STREAMS));
            properties.put(PropertieKeys.FILE_METHOD, Flags.STREAMS);
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("Free memory: " + Runtime.getRuntime().freeMemory()/(1024*1024) + "mb");
        initProperties(args);
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        String fxmlFile = "/window.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream(fxmlFile));
        stage.setTitle("lab2");
        stage.setScene(new Scene(root));
        Window controller = loader.getController();
        EventHandler<WindowEvent> closeEventHandler = windowEvent -> controller.saveWindowPropertiesAndExit(properties);
        EventHandler<KeyEvent> keyEventEventHandler = controller::setKeyEvents;
        stage.setOnCloseRequest(closeEventHandler);
        stage.getScene().setOnKeyPressed(keyEventEventHandler);
        controller.setProperties(properties, stage);
        stage.show();
    }
}
