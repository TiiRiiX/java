package ru.fnight.lab2;

import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Properties {

    public static class PropertieKeys {
        public static String WINDOW_WIDTH = "windowWidth";
        public static String WINDOW_HEIGHT = "windowHeight";
        public static String WINDOW_X = "windowX";
        public static String WINDOW_Y = "windowY";
        public static String BACKGROUND_COLOR = "backgroundColor";
        public static String FILE_METHOD = "fileMethod";
    }

    private HashMap<String, String> map;

    public Properties(String settings) {
        this.map = new HashMap<>();
        parseSettings(settings);
    }

    public Properties() {
        this.map = new HashMap<>();
    }

    private void parseSettings(String settings) {
        Stream.of(settings.split("\n")).forEach(s -> {
            String[] pair = s.split("=");
            try {
                this.put(pair[0], pair[1]);
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("wrong properties: " + s);
            }
        });
    }

    public String get(String key) {
        return map.get(key);
    }

    public void put(String key, String value) {
        map.put(key, value);
    }

    public boolean exist(String key) {
        return map.containsKey(key);
    }

    public Set<String> keys() {
        return map.keySet();
    }

    @Override
    public String toString() {
        return this.keys().stream().map(key -> key + "=" + this.get(key))
                .collect(Collectors.joining("\n"));
    }
}
