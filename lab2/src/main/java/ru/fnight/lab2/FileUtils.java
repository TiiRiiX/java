package ru.fnight.lab2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class FileUtils {

    public static class Flags {
        public static final String MEMORY = "memory";
        public static final String VARS = "vars";
        public static final String STREAMS = "streams";
        // public static String API = "api"; не работает :(
    }

    public static String read(String fileName, String method) {
        String result = "";
        long startWatch = System.nanoTime();
        try {
            switch (method) {
                case Flags.MEMORY:
                    result = readWithBuffer(fileName);
                    break;
                case Flags.VARS:
                    result = readWithReader(fileName);
                    break;
                case Flags.STREAMS:
                    result = readWithStream(fileName);
                    break;
                default:
                    result = readWithStream(fileName);
            }
            System.out.println("Read in: " + (System.nanoTime() - startWatch));
        } catch (IOException e) {
            System.err.println("file not found");
        }
        return result;
    }

    // чтение и запись через отображение на память, флаг - memory
    public static String readWithBuffer(String fileName) throws IOException {
        StringBuilder result = new StringBuilder();
        RandomAccessFile file = new RandomAccessFile(fileName, "r");
        FileChannel channel = file.getChannel();
        long size = channel.size();
        long availableMemory = Runtime.getRuntime().freeMemory();
        long lastPosition = 0;
        while (lastPosition < size) {
            long part = lastPosition + availableMemory < size ? availableMemory : size - lastPosition;
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, lastPosition, part);
            buffer.load();
            for (int i = 0; i < buffer.limit(); i++) {
                result.append((char) buffer.get());
            }
            buffer.clear();
            lastPosition += part;
        }
        channel.close();
        file.close();
        return result.toString();
    }

    public static void writeWithBuffer(String fileName, String string) throws IOException {
        long startWatch = System.nanoTime();
        FileChannel channel = new FileOutputStream(fileName).getChannel();
        channel.write(ByteBuffer.wrap(string.getBytes()));
        channel.close();
        System.out.println("Write in: " + (System.nanoTime() - startWatch));
    }

    // файловые переменные, флаг - vars
    public static String readWithReader(String fileName) throws IOException {
        StringBuilder result = new StringBuilder();
        Reader reader = new FileReader(new File(fileName));
        int data;
        while ((data = reader.read()) != -1) {
            result.append((char) data);
        }
        reader.close();
        return result.toString();
    }

    public static void writeWithWriter(String fileName, String string) throws IOException {
        long startWatch = System.nanoTime();
        FileWriter writer = new FileWriter(new File(fileName));
        writer.write(string);
        writer.close();
        System.out.println("Write in: " + (System.nanoTime() - startWatch));
    }

    // потоки, флаг - streams
    public static String readWithStream(String fileName) throws IOException {
        StringBuilder result = new StringBuilder();
        FileInputStream stream = new FileInputStream(fileName);
        int cur;
        while ((cur = stream.read()) != -1) {
            result.append((char) cur);
        }
        stream.close();
        return result.toString();
    }

    public static void writeWithStream(String fileName, String string) throws IOException {
        long startWatch = System.nanoTime();
        FileOutputStream stream = new FileOutputStream(fileName);
        byte[] bytes = string.getBytes();
        stream.write(bytes, 0, bytes.length);
        System.out.println("Write in: " + (System.nanoTime() - startWatch));
        stream.close();
    }
}
