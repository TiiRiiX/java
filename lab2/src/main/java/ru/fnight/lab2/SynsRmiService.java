package ru.fnight.lab2;

import java.net.URISyntaxException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SynsRmiService extends Remote {

    void createRandomImage(double x, double y, String filePath) throws RemoteException, URISyntaxException;

}
