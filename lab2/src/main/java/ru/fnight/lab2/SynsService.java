package ru.fnight.lab2;

import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SynsService {

    public static final Integer FIRST_PORT = 1234;
    public static int PORT;
    public static final String STRING_PORT_SLITTER = ";";
    public static final String RMI_HOSTNAME = "java.rmi.server.hostname";
    public static final String LOCALHOST = "localhost";

    public static boolean checkPortAvailable(int port) {
        try (Socket ignored = new Socket("localhost", port)) {
            return false;
        } catch (IOException ignored) {
            return true;
        }
    }

    public static int findFreePort(int firstPort) {
        if (!checkPortAvailable(firstPort)) {
            return findFreePort(firstPort + 1);
        } else {
            return firstPort;
        }
    }

    public static List<Integer> findNeighbours() {
        String strPorts = "";
        try {
            strPorts = FileUtils.readWithStream(App.PORTS_FILE_NAME);
        } catch (IOException e) {
            System.err.println("file not found");
        }
        String[] ports = strPorts.split(STRING_PORT_SLITTER);
        if (ports.length > 0 && !ports[0].equals("")) {
            return Stream.of(ports).map(Integer::parseInt)
                    .filter(neighPort -> !neighPort.equals(PORT)).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public static void saveYouToNeighbours(Integer port) {
        List<Integer> neighbours = findNeighbours();
        neighbours.add(port);
        try {
            FileUtils.writeWithStream(App.PORTS_FILE_NAME, String.join(STRING_PORT_SLITTER,
                    neighbours.stream().map(a -> Integer.toString(a)).collect(Collectors.toList())));
        } catch (Exception e) {
            System.err.println("error while writing");
        }
    }

    public static void saveNeighbourWithoutYou(Integer port) {
        List<Integer> neighbours = findNeighbours();
        neighbours.removeIf(port::equals);
        try {
            FileUtils.writeWithStream(App.PORTS_FILE_NAME, String.join(STRING_PORT_SLITTER,
                    neighbours.stream().map(a -> Integer.toString(a)).collect(Collectors.toList())));
        } catch (Exception e) {
            System.err.println("error while writing");
        }
    }

    public static void createServer(AnchorPane pane) {
        try {
            SynsRmiService service = new SynsRmiServiceImpl(pane);
            PORT = findFreePort(FIRST_PORT);
            Registry registry = LocateRegistry.createRegistry(PORT);
            registry.rebind(SynsRmiServiceImpl.serviceName, service);
            System.out.println("Server created at " + PORT);
            saveYouToNeighbours(PORT);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    public static void sayToCreateImage(double x, double y, String filePath) {
        findNeighbours().forEach(port -> {
            try {
                String address = "rmi://" + LOCALHOST + ":" + port.toString() + "/" + SynsRmiServiceImpl.serviceName;
                SynsRmiService service;
                service = (SynsRmiService) Naming.lookup(address);
                service.createRandomImage(x, y, filePath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
