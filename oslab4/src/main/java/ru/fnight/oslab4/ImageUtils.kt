package ru.fnight.oslab4

import javafx.embed.swing.SwingFXUtils
import javafx.scene.image.Image
import java.awt.image.BufferedImage
import java.awt.image.DataBufferInt
import java.io.File
import javax.imageio.ImageIO

class ImageUtils {

    companion object {

        @JvmStatic
        fun getJavaFxImage(pathToFile: String): Image {
            val imgInfo: ImgInfo = getImageInfo(pathToFile)
            val image = BufferedImage(imgInfo.width, imgInfo.height, BufferedImage.TYPE_INT_ARGB)
            val imagePixels: IntArray = (image.raster.dataBuffer as DataBufferInt).data
            System.arraycopy(imgInfo.pixels, 0,imagePixels, 0, imgInfo.pixels.size)
            image.flush()
            return SwingFXUtils.toFXImage(image, null)
        }

        @JvmStatic
        private fun getImageInfo(pathToFile: String): ImgInfo {
            val bi: BufferedImage = ImageIO.read(File(pathToFile))
            val p = BufferedImage(bi.width, bi.height, BufferedImage.TYPE_INT_ARGB)
            p.graphics.drawImage(bi, 0, 0, null)
            val pixels: IntArray = (p.raster.dataBuffer as DataBufferInt).data
            bi.flush()
            p.flush()
            return ImgInfo(pixels, bi.width, bi.height)
        }

    }

}