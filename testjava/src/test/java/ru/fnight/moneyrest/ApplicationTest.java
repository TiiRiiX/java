package ru.fnight.moneyrest;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;



@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void stage1_checkDataBase() throws Exception {
        this.mockMvc.perform(get("/bank/")).andExpect(status().isOk());
        this.mockMvc.perform(get("/client/")).andExpect(status().isOk());
        this.mockMvc.perform(get("/deposit/")).andExpect(status().isOk());
    }
    
    @Test
    public void stage1_saveSomeDataToClient() throws Exception {
        this.mockMvc.perform(post("/client/?fullName=FullName1&shortName=ShortName1&address=Address1&form=123"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.fullName", is("FullName1")));
        this.mockMvc.perform(post("/client/?fullName=FullName2&shortName=ShortName2&address=Address2&form=321"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.fullName", is("FullName2")));
        this.mockMvc.perform(post("/client/?fullName=FullName3&shortName=ShortName3&address=Address3&form=456"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.fullName", is("FullName3")));
    }
    
    @Test
    public void stage1_saveSomeDataToBank() throws Exception {
        this.mockMvc.perform(post("/bank/?name=Bank1&bic=123467"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name", is("Bank1")));
        this.mockMvc.perform(post("/bank/?name=Bank2&bic=46778"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name", is("Bank2")));
        this.mockMvc.perform(post("/bank/?name=Bank3&bic=183467"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name", is("Bank3")));
    }
    
    @Test
    public void stage2_getAllClients() throws Exception {
        this.mockMvc.perform(get("/client/"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(3)));
    }
    
    @Test
    public void stage2_getAllBanks() throws Exception {
        this.mockMvc.perform(get("/bank/"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(3)));
    }
    
    @Test
    public void stage3_deleteClient() throws Exception {
        this.mockMvc.perform(delete("/client/1"))
            .andExpect(status().isOk())
            .andExpect(content().string("Client was deleted"));
        this.mockMvc.perform(get("/client/"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)));
    }
    
    @Test
    public void stage3_deleteBank() throws Exception {
        this.mockMvc.perform(delete("/bank/1"))
            .andExpect(status().isOk())
            .andExpect(content().string("Bank was deleted"));
        this.mockMvc.perform(get("/bank/"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)));
    }
    
    @Test
    public void stage3_updateClient() throws Exception {
        this.mockMvc.perform(put("/client/2?fullName=NewFullName&shortName=NewShortName&address=NewAdress&form=123"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(2)))
            .andExpect(jsonPath("$.fullName", is("NewFullName")))
            .andExpect(jsonPath("$.shortName", is("NewShortName")))
            .andExpect(jsonPath("$.address", is("NewAdress")))
            .andExpect(jsonPath("$.form", is(123)));
    }
    
    @Test
    public void stage3_updateBank() throws Exception {
        this.mockMvc.perform(put("/bank/2?name=NewBank&bic=12341241"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(2)))
            .andExpect(jsonPath("$.name", is("NewBank")))
            .andExpect(jsonPath("$.bic", is("12341241")));
    }
    
    @Test
    public void stage4_createSeveralDeposits() throws Exception {
        this.mockMvc.perform(post("/deposit/?idClient=2&idBank=3&date=02.2018&procent=20&period=12.2"))
            .andExpect(status().isOk());
        this.mockMvc.perform(post("/deposit/?idClient=3&idBank=2&date=04.2017&procent=15&period=8"))
            .andExpect(status().isOk());
    }
    
    @Test
    public void stage5_checkDeposits() throws Exception {
        this.mockMvc.perform(get("/deposit/"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)));
    }
    
    @Test
    public void stage6_updateDeposit() throws Exception {
        this.mockMvc.perform(put("/deposit/1?idClient=3&idBank=2&date=01.2015&procent=100&period=0.2"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.idClient", is(3)))
            .andExpect(jsonPath("$.idBank", is(2)))
            .andExpect(jsonPath("$.date", is("01.2015")))
            .andExpect(jsonPath("$.procent", is(100.0)))
            .andExpect(jsonPath("$.period", is(0.2)));
    }
    
    @Test
    public void stage6_deleteDeposit() throws Exception {
        this.mockMvc.perform(delete("/deposit/2"))
            .andExpect(status().isOk())
            .andExpect(content().string("Deposit was deleted"));
        this.mockMvc.perform(get("/deposit/"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)));
    }
}