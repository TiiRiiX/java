package ru.fnight.moneyrest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Deposit {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private long idClient;
    private long idBank;
    private String date;
    private float procent;
    private float period;
    
    protected Deposit() {}

    public Deposit(long idClient, long idBankm, String date, float procent, float period) {
        this.idClient = idClient;
        this.idBank = idBank;
        this.date = date;
        this.procent = procent;
        this.period = period;
    }
    
    public long getId() {
        return id;
    }
    
    public long getIdClient() {
        return idClient;
    }
    public void setIdClient(long idClient) {
        this.idClient = idClient;
    }
    
    public long getIdBank() {
        return idBank;
    }
    public void setIdBank(long idBank) {
        this.idBank = idBank;
    }
    
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    
    public float getProcent() {
        return procent;
    }
    public void setProcent(float procent) {
        this.procent = procent;
    }
    
    public float getPeriod() {
        return period;
    }
    public void setPeriod(float period) {
        this.period = period;
    }
}