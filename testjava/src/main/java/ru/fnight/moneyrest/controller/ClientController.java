package ru.fnight.moneyrest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import ru.fnight.moneyrest.model.Client;
import ru.fnight.moneyrest.repository.ClientRepository;

@RestController
@RequestMapping("/client")
public class ClientController {
    
    @Autowired
    private ClientRepository repository;

    @PostMapping("/")
    public Client createClient(@RequestParam String fullName, @RequestParam String shortName, @RequestParam String address, @RequestParam int form) {
		return repository.save(new Client(fullName, shortName, address, form));
    }
    
    @GetMapping("/")
    public Iterable<Client> allClients() {
		return repository.findAll();
    }
    
    @GetMapping("/{id}")
    public Client oneClientById(@PathVariable("id") long id) {
        return repository.findOne(id);
    }
    
    @DeleteMapping("/{id}")
    public String deleteClientById(@PathVariable("id") long id) {
        repository.delete(repository.findOne(id));
        return "Client was deleted";
    }
    
    @PutMapping("/{id}")
    public Client updateClient(@RequestParam String fullName, @RequestParam String shortName, @RequestParam String address, @RequestParam int form, @PathVariable long id) {
        Client client = repository.findOne(id);
        client.setFullName(fullName);
        client.setShortName(shortName);
        client.setAddress(address);
        client.setForm(form);
        return repository.save(client);
    }
}