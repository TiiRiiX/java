package ru.fnight.moneyrest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import ru.fnight.moneyrest.model.Bank;
import ru.fnight.moneyrest.repository.BankRepository;

@RestController
@RequestMapping("/bank")
public class BankController {
    
    @Autowired
    private BankRepository repository;

    @PostMapping("/")
    public Bank createClient(@RequestParam String name, @RequestParam String bic) {
			return repository.save(new Bank(name, bic));
    }
    
    @GetMapping("/")
    public Iterable<Bank> allBanks() {
			return repository.findAll();
    }
    
    @GetMapping("/{id}")
    public Bank oneBankById(@PathVariable("id") long id) {
        return repository.findOne(id);
    }
    
    @DeleteMapping("/{id}")
    public String deleteBankById(@PathVariable("id") long id) {
        repository.delete(repository.findOne(id));
        return "Bank was deleted";
    }
    
    @PutMapping("/{id}")
    public Bank updateBank(@RequestParam String name, @RequestParam String bic, @PathVariable long id) {
        Bank bank = repository.findOne(id);
        bank.setName(name);
        bank.setBic(bic);
        return repository.save(bank);
    }
}