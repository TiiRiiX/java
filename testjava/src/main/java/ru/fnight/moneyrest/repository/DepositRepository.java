package ru.fnight.moneyrest.repository;

import org.springframework.data.repository.CrudRepository;

import ru.fnight.moneyrest.model.Deposit;

public interface DepositRepository extends CrudRepository<Deposit, Long> {

}