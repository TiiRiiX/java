package ru.fnight.moneyrest.repository;

import org.springframework.data.repository.CrudRepository;

import ru.fnight.moneyrest.model.Client;

public interface ClientRepository extends CrudRepository<Client, Long> {

}